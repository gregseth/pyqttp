FROM python:3.7

# Python dev tools
RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y expect
RUN apt-get install -y jq

COPY requirements.txt ./
RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 install --no-cache-dir twine keyrings.cryptfile
RUN pip3 install --no-cache-dir -r requirements.txt
