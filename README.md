# PyQt Template Project

Templates files to build, run, test and deploy a PyQt project on gitlab.com.


## How to use

Just run:
```bash
git clone https://gitlab.com/gregseth/pyqttp your_project
cd your_project
./init.sh
```

## What it does

-   creates a `setup.py` for publication on PyPI;
-   creates a subdirectory with named after the project with minimal `__init__.py` and `__main__.py ` to start coding your PyQt application;
-   includes a `.gitlab-ci.yml` to use CI features provided by gitlab.com (see [CI Workflow](#CI-Workflow));
-   includes scripts to build the project;
-   includes VSCode settings and tasks relying on the build scripts.


## What it does not (and must be configured manually)

On gitlab.com, you must provide the following variables in the CI config of the project (it is **strongly** advised to mark them as masked):
-   `TOKEN`: a GitLab private access token for the project;
-   `PYPI_USER`: the PyPI account name used to publish the project (or `__token__` if using a PyPI access token);
-   `PYPI_PASS`: the password of the account, or the token.


## CI Workflow

```mermaid
graph LR
  subgraph Containerize
    C1[base docker image]
    Cn[docker image n]
  end
  C1 --> B[build]
  Cn -.-> B[build]
  B --> P[package]
  subgraph Tests
    TI[Test install] --> Tok{OK}
    TU[Unit tests] -.-> Tok
    TT[Integration tests] -.-> Tok
  end
  P --> TI
  P -.-> TU
  P -.-> TT
  subgraph Publish release
    R[GitLab]
    PyPI[PyPI]
  end
  Tok --> R
  Tok --> PyPI
```
_Steps linked with dashed arrows are not provided by this template._

### Containerize
Generates docker containers for the subsequent steps. The provided base container has all the tools required by the subsequent steps to work properly. If you want to add a container for a specific step of the worflow (e.g. unit tests) create the `awesome-unit-tests.dockerfile` in the `buildtools` directory, and add the following lines in the `.gitlab-ci.yml`:

```yml
awesome-unit-tests_image:
  <<: *template
  only:
    changes:
      - buildtools/awesome-unit-tests.dockerfile
  except:
    - tags
```

Then the test job must have the following lines:

```yml
  image: $REGISTRY/awesome-unit-tests
```

### Build
Generates intermediate files. By default compiles `.ui` files.

### Package
Generates the source and binary packages (resp. `.tar.gz` and `.whl`) for installation with `pip`.

**Note:** For this step to succeed `git describe` command must succeed. That means that the project must at least have benn tagged once.

### Test
Runs tests. By default only test the success of the installation of the generated package.

### Publish
Publishes the package to the GitLab release page of the project and on PyPI.

**Note:** The release on GitLab is only triggered by tags (any). The release on PyPI is only triggerd for tags matching `/^v\d+\.\d+\.\d+/` (vX.Y.Z).
