import sys
import logging as log

from PyQt5.QtWidgets import QApplication

def main():
    stream_handler = log.StreamHandler()
    log.basicConfig(handlers=(stream_handler,),
                    level='DEBUG',
                    format='%(asctime)s.%(msecs)03d:%(levelname)-8s:%(module)-12s# %(message)s',
                    datefmt='%Y%m%d-%H%M%S'
                    )

    APP = QApplication(sys.argv)

    sys.exit(APP.exec_())


if __name__ == '__main__':
    main()
