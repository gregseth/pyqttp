#!/usr/bin/env bash

read -p "Project name: " project
read -p "Project short description: " desc
read -p "Project GitLab group (GilLab username for personnal projects): " nick
read -p "Author full name: " fullname
read -p "Author email: " email

echo "Project files configuration..."
sed -i'' -r "s/{{PROJECT}}/$project/" buildtools/* .vscode/*
mv project "$project"
cat <<EOF >"$project/__init__.py"
""" $desc """

__version__ = "{{VERSION}}"
__author__ = "$fullname"

EOF
mv gitlab-ci.yml .gitlab-ci.yml
cat <<EOF >README.md
$project
==========================================

$desc
EOF


echo "Generating setup.py..."
cat <<EOF >setup.py
import setuptools

import $project

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    install_requires = [line.strip() for line in fh]

setuptools.setup(
    name="$project",
    version=$project.__version__,
    author=$project.__author__,
    author_email="$email",
    description="$desc",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/$nick/$project",
    packages=setuptools.find_packages(),
    install_requires=install_requires,
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
    ],
    entry_points={
        'console_scripts': [
            '$project = $project.__main__:main'
        ]
    }
)

EOF

rm -rf .git
git init .
echo "Adding remote repository..."
git remote add origin "git@gitlab.com:$nick/$project.git"
git add .
echo "Creating initial commit..."
git commit -a -m "Initial commit (base files)."

# Cleanup
rm -- "$0"
echo "Done."
